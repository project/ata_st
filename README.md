# Admin Table Alter

Admin Table Alter makes it possible to find modules within your Drupal installation. It is allow to
alter admin side table columns according content type fields.

Our goal is to make it easier to find and install modules for people new to Drupal and site builders.
Developers will also find this valuable since it provides the composer commands to get the modules.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ata_st).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/3331318).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Sumit Thakur - [sumit_thakur](https://www.drupal.org/u/sumit_thakur)