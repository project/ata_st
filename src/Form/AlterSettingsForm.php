<?php

namespace Drupal\alter_admin_table\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure nodeviewcount settings.
 */
class AlterSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a AlterSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alter_admin_table_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['alter_admin_table.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('alter_admin_table.settings');
    $form['content_types'] = [
      '#title' => $this->t('Content types'),
      '#description' => $this->t('Choose content types to count views on its node.'),
      '#type' => 'checkboxes',
      '#options' => $this->getNodeTypesOptions(),
      '#default_value' => $config->get('node_types') ? $config->get('node_types') : [],
    ];

    $form['start_from'] = [
      '#type' => 'number',
      '#title' => t('Start from'),
      '#description' => t('Start count from set number'),
      '#default_value' => $config->get('start_from') ? $config->get('start_from') : 0,
    ];

    $form['logs_discard_after'] = [
      '#type' => 'select',
      '#title' => $this->t('Discard node views logs after'),
      '#default_value' => $config->get('logs_discard_after'),
      '#options' => $this->getLogsLifeTimeOptions(),
      '#description' => $this->t('Older log entries will be automatically discarded, (Requires a correctly configured <a href="@cron">cron maintenance task</a>.). Pick Never if you dont want logs to be deleted.',
        ['@cron' => Url::fromRoute('system.status')->toString()]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Summary of getNodeTypesOptions.
   *
   * @return array
   *   A string containing a URL that may be used to access the file.
   */
  private function getNodeTypesOptions() {
    $node_types_options = [];
    $node_types = $this->entityTypeManager->getStorage('node_type')
      ->loadMultiple();
    foreach ($node_types as $node_id => $node_type) {
      $node_types_options[$node_id] = $node_type->label();
    }
    return $node_types_options;
  }

  /**
   * Get logs lifetime options.
   *
   * @return array
   *   Logs lifetime options.
   */
  private function getLogsLifeTimeOptions() {
    $life_time_options = [0 => $this->t('Never')];
    $time_intervals = [
      86400,
      604800,
      1209600,
      2592000,
      15552000,
      31536000,
    ];
    foreach ($time_intervals as $time_interval) {
      $life_time_options[$time_interval] = $this->dateFormatter->formatInterval($time_interval);
    }
    return $life_time_options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('alter_admin_table.settings')
      ->set('node_types', array_keys(array_filter($form_state->getValue('content_types'))))
      ->set('start_from', $form_state->getValue('start_from'))
      ->set('logs_discard_after', (int) $form_state->getValue('logs_discard_after'))
      ->save(TRUE);
    $this->cacheTagsInvalidator->invalidateTags(['node_view']);
    parent::submitForm($form, $form_state);
  }

}
