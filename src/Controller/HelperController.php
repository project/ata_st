<?php

namespace Drupal\alter_admin_table\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for the salutation message.
 */
class HelperController extends ControllerBase {

  /**
   * Hello World.
   *
   * @return array
   *   Our message.
   */
  public function test() {
    return [
      '#markup' => $this->t('Hello World'),
    ];
  }

}
